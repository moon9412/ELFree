<?
include("admin/inc.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" dir="ltr">
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>エントリーありがとうございました｜SEES</title>
	<meta name="description" content="各種求人案件のエントリーフォーム。">
	<meta name="keywords" content="IT,求人,検索,登録面談,エントリー">
	<!-- for legacy browser -->
	<link rel="stylesheet" type="text/css" href="resource/css/legacy.css">
	<!-- for modern browser -->
	<link rel="stylesheet" type="text/css" href="resource/css/modern.css" media="screen,print">
	<link rel="stylesheet" type="text/css" href="resource/css/font.css" media="screen,print" title="default">
	<link rel="stylesheet" type="text/css" href="css/index.css" media="screen,print" title="default">
	<link rel="stylesheet" type="text/css" href="resource/css/nivo-slider.css" media="screen,print" title="default">
	<link rel="stylesheet" type="text/css" href="resource/css/themes/default/default.css" media="screen,print" title="default">
	<!-- load JavaScript -->
	<script type="text/javascript" src="resource/script/common.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript" charset="utf-8">
    <!--
    google.load("jquery", "1");
    -->
    </script>
    <script type="text/javascript" src="resource/script/jquery.nivo.slider.pack.js"></script> 
    <script>
	$(window).load(function() {
        $('#slider').nivoSlider({
								effect:'random',
								pauseTime:10000
								});
    });
	
	$(function() {
		
		// Twitter
	function date_format(date){
	    function format(str){
	        return ("00" + str).slice(-2);
	    }
	    var array = date.split(" ");
	    date = new Date(array[1] + " " + array[2] + ", " + array[5] + " " + array[3]);
		date.setHours(date.getHours() + 9);
	    return date.getFullYear()  + "/" +
		       format(date.getMonth() + 1) + "/" +
	           format(date.getDate())      + " " +
	           format(date.getHours())     + ":" +
	           format(date.getMinutes());
	}
	$.getJSON('http://twitter.com/statuses/user_timeline/miraie_sees.json?count=3&callback=?', function (json) {
		for(var i in json){
			var text = json[i].text;
			var created_at = json[i].created_at;
			var twdate = date_format(created_at);
			text = text.replace(/(https?:\/\/[-a-z0-9._~:\/?#@!$&amp;amp;amp;\'()*+,;=%]+)/ig,'<a href="$1" target="_blank">$1</a>').replace(/@+([_A-Za-z0-9-]+)/ig,'<a href="http://twitter.com/$1" target="_blank">@$1</a>').replace(/#+([_A-Za-z0-9-]+)/ig,'<a href="http://search.twitter.com/search?q=$1" target="_blank">#$1</a>');
			$("#twitterArea").append('<span class="fwb txt10">' + twdate + '</span>' + '<p class="">' + text + '</p>');
		}
	});
			   
	});
	
	</script>
    <script type="text/javascript" src="//platform.twitter.com/widgets.js" defer="defer"></script>
    <script type="text/javascript" src="resource/script/jkl-calendar.js" charset="UTF-8"></script>
    <script>
    var cal = new JKL.Calendar("calid","formid","colname");
    var cal2 = new JKL.Calendar("calid2","formid","colname2");
    </script>
    
</head>
<body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37830642-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
	<!-- skip sec/ -->
	<div id="skiplinkSec">
		<ul>
			<li><a href="#headAnc">ヘッダーへジャンプします</a></li>
			<li><a href="#naviAnc">メニューへジャンプします</a></li>
			<li><a href="#mainAnc">本文へジャンプします</a></li>
		</ul>
	</div>
	<!-- /skip sec -->

<div id="container">

	<p><a name="headAnc" id="headAnc">ヘッダーのはじまりです</a></p>
	
	<div id="headSec">
    	<div id="head1">シニアエンジニア向けの求人案件検索サイト、随時更新中！</div>
        <div id="head2" class="clearfix">
        	<div class="floatLeft w300 m-t1"><a href="index.html"><img src="resource/img_common/0120logo.png" alt="シニアエンジニア向けの求人案件検索サイト｜SEES"></a></div>
            <div class="floatLeft w600 alignRight m-t1"><img src="resource/img_common/headtel2.png"></div>
        </div>
        <div id="head3" class="clearfix">
        	<ul>
            <li><a href="index.html"><img src="resource/img_common/navi1.png" alt="トップページ" class="imgover"></a></li>
            <li><a href="about.html"><img src="resource/img_common/navi2.png" class="imgover"></a></li>
            <li><a href="flow.html"><img src="resource/img_common/navi3.png" alt="就業までのフロー" class="imgover"></a></li>
            <li><a href="list.html"><img src="resource/img_common/navi4.png" alt="新着求人案件" class="imgover"></a></li>
            <li><a href="company.html"><img src="resource/img_common/navi5.png" alt="運営会社" class="imgover"></a></li>
            <li><a href="inquiry.html"><img src="resource/img_common/navi6.png" alt="お問い合わせ" class="imgover"></a></li>
            </ul>
        </div>
	<!-- /▲head sec -->
	</div>
	
	<hr />
	
	<div id="bodySec" class="clearfix">
    	
        <div id="main">
        	
            <p><img src="img/entry/title.png" alt="求人案件エントリー" width="648" height="56"></p>
            
    <div class="m-t1 m-b2"><a href="index.html">シニアエンジニア向けの求人サイトTOP</a> > <a href="list.html">求人案件一覧</a> &gt; <a href="detail<? echo $_GET[id]; ?>.html">詳細</a> &gt; エントリー</div>
            
              <p class="m-b2 f15 bold">エントリーありがとうございました。</p>
              
              <p class="m-b2">この度はエントリーありがとうございます。<br>
              折り返し担当者よりご連絡させて頂きます。</p>
    
              <p class="m-b2">なお、お問い合わせ順に返信致しておりますが<br>
    多くのお問い合わせを頂いている場合は返信が遅れる場合があります。<br>
    予めご了承下さい。<br>
    <br>
    &gt;&gt; <a href="index.html">トップページへ戻る</a></p>

        </div>
       
        <div id="sub">
            <?
			include("side.php");
			?>
        </div>

	<!-- /▲body sec --></div>

<!-- /container--></div>

<div id="pagetop" class="p-t5"><a href="javascript:backToTop();"><img src="resource/img_common/pagetop.png"></a></div>

<div id="footSec" class="clearfix">
	
    <div id="footin" class="p-t15">    <div class="floatLeft w330"><a href="index.html"><img src="resource/img_common/0120logo2.png" alt="シニアエンジニア向けの求人案件検索サイト｜SEES"></a></div>
    <div class="floatLeft w370 p-t15 f11 alignLeft">シニアエンジニア向けの求人案件検索サイト、随時更新中！</div>
    <div class="floatLeft w190"><img src="resource/img_common/foottel.png"></div>
</div>
    
    <div id="footin2" class="p-t2">
		
        <div class="floatLeft footcom alignLeft">
        <p class="f13 bold">株式会社Miraie</p>
        <p class="f11 m-t05">〒150－0002</p>
        <p class="f11">東京都渋谷区渋谷1－12－12 東豊エステートビル8F</p>
        <p class="f11 m-t05">TEL 03-5774-6300　FAX 03-5774-6301</p>
        </div>
        
        <div class="floatLeft w580 ml20 alignLeft p-t05">
        <p class="aline p-b1"><img src="resource/img_common/service.png" alt="Service"></p>
        <p class="p-t1">
        <img src="resource/img_common/flow2.png" class="imgcenter"> <a href="list.html" herf="">新着案件</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="about.html">SEESとは</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="flow.html">就業までのフロー</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="company.html">運営会社</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="inquiry.html">お問い合わせ</a>
        </p>
        </div>
        
	</div>
        
<!-- /foot sec -->
</div>

</body>
</html>
