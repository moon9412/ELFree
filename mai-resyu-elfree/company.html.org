<?
include("admin/inc.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" dir="ltr">
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>運営会社｜SEES</title>
	<meta name="description" content="SEESを運営する株式会社Miraieについてのご紹介。">
	<meta name="keywords" content="IT,求人,検索,エンジニア">
	<!-- for legacy browser -->
	<link rel="stylesheet" type="text/css" href="resource/css/legacy.css">
	<!-- for modern browser -->
	<link rel="stylesheet" type="text/css" href="resource/css/modern.css" media="screen,print">
	<link rel="stylesheet" type="text/css" href="resource/css/font.css" media="screen,print" title="default">
	<link rel="stylesheet" type="text/css" href="css/index.css" media="screen,print" title="default">
	<link rel="stylesheet" type="text/css" href="resource/css/nivo-slider.css" media="screen,print" title="default">
	<link rel="stylesheet" type="text/css" href="resource/css/themes/default/default.css" media="screen,print" title="default">
	<!-- load JavaScript -->
	<script type="text/javascript" src="resource/script/common.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript" charset="utf-8">
    <!--
    google.load("jquery", "1");
    -->
    </script>
    <script type="text/javascript" src="resource/script/jquery.nivo.slider.pack.js"></script> 
    <script>
	$(window).load(function() {
        $('#slider').nivoSlider({
								effect:'random',
								pauseTime:10000
								});
    });
	
	$(function() {
		
		// Twitter
	function date_format(date){
	    function format(str){
	        return ("00" + str).slice(-2);
	    }
	    var array = date.split(" ");
	    date = new Date(array[1] + " " + array[2] + ", " + array[5] + " " + array[3]);
		date.setHours(date.getHours() + 9);
	    return date.getFullYear()  + "/" +
		       format(date.getMonth() + 1) + "/" +
	           format(date.getDate())      + " " +
	           format(date.getHours())     + ":" +
	           format(date.getMinutes());
	}
	$.getJSON('http://twitter.com/statuses/user_timeline/miraie_sees.json?count=3&callback=?', function (json) {
		for(var i in json){
			var text = json[i].text;
			var created_at = json[i].created_at;
			var twdate = date_format(created_at);
			text = text.replace(/(https?:\/\/[-a-z0-9._~:\/?#@!$&amp;amp;amp;\'()*+,;=%]+)/ig,'<a href="$1" target="_blank">$1</a>').replace(/@+([_A-Za-z0-9-]+)/ig,'<a href="http://twitter.com/$1" target="_blank">@$1</a>').replace(/#+([_A-Za-z0-9-]+)/ig,'<a href="http://search.twitter.com/search?q=$1" target="_blank">#$1</a>');
			$("#twitterArea").append('<span class="fwb txt10">' + twdate + '</span>' + '<p class="">' + text + '</p>');
		}
	});
			   
	});
	
	</script>
    <script type="text/javascript" src="//platform.twitter.com/widgets.js" defer="defer"></script>
    
</head>
<body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37830642-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
	<!-- skip sec/ -->
	<div id="skiplinkSec">
		<ul>
			<li><a href="#headAnc">ヘッダーへジャンプします</a></li>
			<li><a href="#naviAnc">メニューへジャンプします</a></li>
			<li><a href="#mainAnc">本文へジャンプします</a></li>
		</ul>
	</div>
	<!-- /skip sec -->

<div id="container">

	<p><a name="headAnc" id="headAnc">ヘッダーのはじまりです</a></p>
	
	<div id="headSec">
    	<div id="head1">シニアエンジニア向けの求人案件検索サイト、随時更新中！</div>
        <div id="head2" class="clearfix">
        	<div class="floatLeft w300 m-t1"><a href="index.html"><img src="resource/img_common/0120logo.png" alt="シニアエンジニア向けの求人案件検索サイト｜SEES"></a></div>
            <div class="floatLeft w600 alignRight m-t1"><img src="resource/img_common/headtel2.png"></div>
        </div>
        <div id="head3" class="clearfix">
        	<ul>
            <li><a href="index.html"><img src="resource/img_common/navi1.png" alt="トップページ" class="imgover"></a></li>
            <li><a href="about.html"><img src="resource/img_common/navi2.png" class="imgover"></a></li>
            <li><a href="flow.html"><img src="resource/img_common/navi3.png" alt="就業までのフロー" class="imgover"></a></li>
            <li><a href="list.html"><img src="resource/img_common/navi4.png" alt="新着求人案件" class="imgover"></a></li>
            <li><a href="company.html"><img src="resource/img_common/navi5_o.png" alt="運営会社"></a></li>
            <li><a href="inquiry.html"><img src="resource/img_common/navi6.png" alt="お問い合わせ" class="imgover"></a></li>
            </ul>
        </div>
	<!-- /▲head sec -->
	</div>
	
	<hr />
	
	<div id="bodySec" class="clearfix">
    	
        <div id="main">
        	
            <p><img src="img/company/title.png" alt="運営会社"></p>
            
            <div class="m-t1 m-b2"><a href="index.html">シニアエンジニア向けの求人サイトTOP</a> > 運営会社</div>
            
            <p class="m-b2"><img src="img/company/img1.png" alt="会社概要"></p>
            
            <p>
            	
                <table width="645" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <th width="145">社名</th>
                    <td width="500">株式会社Miraie</td>
                  </tr>
                  <tr>
                    <th width="145">所在地</th>
                    <td width="500">〒150－0002<br>
                    東京都渋谷区渋谷1－12－12　東豊エステートビル8F</td>
                  </tr>
                  <tr>
                    <th width="145">設立</th>
                    <td width="500">2007年7月（3月決算）</td>
                  </tr>
                  <tr>
                    <th width="145">電話</th>
                    <td width="500">03-5774-6300</td>
                  </tr>
                  <tr>
                    <th width="145">FAX</th>
                    <td width="500">03-5774-6301</td>
                  </tr>
                  <tr>
                    <th width="145">代表取締役社長</th>
                    <td width="500">岡野 徹志</td>
                  </tr>
                  <tr>
                    <th width="145">お取引企業一覧</th>
                    <td width="500">富士ソフト㈱<br>
　　　　　　　　　　　㈱帝国データバンクアクシス<br>
　　　　　　　　　　　㈱アドービジネスコンサルタント<br>
　　　　　　　　　　　㈱エスアイインフォジェニック<br>
　　　　　　　　　　　㈱ニッセルワン<br>
　　　　　　　　　　　<br>
　　　　　　　　　　　他、大手・中堅SIer、優良SWH様</td>
                  </tr>
                  <tr>
                    <th width="145">アクセス</th>
                    <td width="500">
                    <iframe width="500" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.jp/maps?f=q&amp;source=s_q&amp;hl=ja&amp;geocode=&amp;q=%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E6%B8%8B%E8%B0%B71%EF%BC%8D12%EF%BC%8D12&amp;aq=&amp;sll=35.678523,139.730784&amp;sspn=0.114202,0.186081&amp;brcurrent=3,0x60188b588cd34965:0x53b9fe8f5a627a1e,0,0x60188b586256530b:0xf37539b10959e052&amp;ie=UTF8&amp;hq=&amp;hnear=%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E6%B8%8B%E8%B0%B7%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%EF%BC%92%E2%88%92%EF%BC%91%EF%BC%92&amp;t=m&amp;z=14&amp;ll=35.660069,139.704356&amp;output=embed&amp;iwloc=B"></iframe><br /><small><a href="https://maps.google.co.jp/maps?f=q&amp;source=embed&amp;hl=ja&amp;geocode=&amp;q=%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E6%B8%8B%E8%B0%B71%EF%BC%8D12%EF%BC%8D12&amp;aq=&amp;sll=35.678523,139.730784&amp;sspn=0.114202,0.186081&amp;brcurrent=3,0x60188b588cd34965:0x53b9fe8f5a627a1e,0,0x60188b586256530b:0xf37539b10959e052&amp;ie=UTF8&amp;hq=&amp;hnear=%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E6%B8%8B%E8%B0%B7%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%EF%BC%92%E2%88%92%EF%BC%91%EF%BC%92&amp;t=m&amp;z=14&amp;ll=35.660069,139.704356" style="color:#0000FF;text-align:left">大きな地図で見る</a></small>
                    </td>
                  </tr>
                </table>
            
            </p>

        </div>
       
        <div id="sub">
            <?
			include("side.php");
			?>
        </div>

	<!-- /▲body sec --></div>

<!-- /container--></div>

<div id="pagetop" class="p-t5"><a href="javascript:backToTop();"><img src="resource/img_common/pagetop.png"></a></div>

<div id="footSec" class="clearfix">
	
    <div id="footin" class="p-t15">    <div class="floatLeft w330"><a href="index.html"><img src="resource/img_common/0120logo2.png" alt="シニアエンジニア向けの求人案件検索サイト｜SEES"></a></div>
    <div class="floatLeft w370 p-t15 f11 alignLeft">シニアエンジニア向けの求人案件検索サイト、随時更新中！</div>
    <div class="floatLeft w190"><img src="resource/img_common/foottel.png"></div>
</div>
    
    <div id="footin2" class="p-t2">
		
        <div class="floatLeft footcom alignLeft">
        <p class="f13 bold">株式会社Miraie</p>
        <p class="f11 m-t05">〒150－0002</p>
        <p class="f11">東京都渋谷区渋谷1－12－2 クロスオフィス渋谷4F</p>
        <p class="f11 m-t05">TEL 03-5774-6300　FAX 03-5774-6301</p>
        </div>
        
        <div class="floatLeft w580 ml20 alignLeft p-t05">
        <p class="aline p-b1"><img src="resource/img_common/service.png" alt="Service"></p>
        <p class="p-t1">
        <img src="resource/img_common/flow2.png" class="imgcenter"> <a href="list.html" herf="">新着案件</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="about.html">SEESとは</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="flow.html">就業までのフロー</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="company.html">運営会社</a>
        <img src="resource/img_common/flow2.png" class="imgcenter ml20"> <a href="inquiry.html">お問い合わせ</a>
        </p>
        </div>
        
	</div>
        
<!-- /foot sec -->
</div>

</body>
</html>