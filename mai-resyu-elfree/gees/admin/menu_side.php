<ul class="nav nav-tabs nav-stacked">
	<li><a href="kyujin_input.html">求人案件の登録</a></li>
	<li><a href="kyujin_list.html">求人案件の編集</a></li>

	<li><a href="gengo_input.html">言語マスタの登録</a></li>
	<li><a href="gengo_list.html">言語マスタの編集</a></li>

	<li><a href="skill_input.html">インフラスキルマスタの登録</a></li>
	<li><a href="skill_list.html">インフラスキルマスタの編集</a></li>

	<li><a href="potision_input.html">ポジションマスタの登録</a></li>
	<li><a href="potision_list.html">ポジションマスタの編集</a></li>

	<li><a href="logout.html">ログアウト</a></li>
</ul>
