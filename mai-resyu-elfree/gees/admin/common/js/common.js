/*
	Standards Compliant Rollover Script
	Author : Daniel Nolan
	http://www.bleedingego.co.uk/webdev.php
*/

function initRollovers() {
	if (!document.getElementById) return
	
	var aPreLoad = new Array();
	var sTempSrc;
	var aImages = document.getElementsByTagName('img');

	for (var i = 0; i < aImages.length; i++) {		
		if (aImages[i].className == 'ro') {
			var src = aImages[i].getAttribute('src');
			var ftype = src.substring(src.lastIndexOf('.'), src.length);
			var hsrc = src.replace(ftype, '_r'+ftype);

			aImages[i].setAttribute('hsrc', hsrc);
			
			aPreLoad[i] = new Image();
			aPreLoad[i].src = hsrc;
			
			aImages[i].onmouseover = function() {
				sTempSrc = this.getAttribute('src');
				this.setAttribute('src', this.getAttribute('hsrc'));
			}	
			
			aImages[i].onmouseout = function() {
				if (!sTempSrc) sTempSrc = this.getAttribute('src').replace('_r'+ftype, ftype);
				this.setAttribute('src', sTempSrc);
			}
		}
	}
}

try{
	window.addEventListener("load",initRollovers,false);
}catch(e){
	window.attachEvent("onload",initRollovers);
}


function backToTop() {
var x1 = x2 = x3 = 0;
var y1 = y2 = y3 = 0;
if (document.documentElement) {
x1 = document.documentElement.scrollLeft || 0;
y1 = document.documentElement.scrollTop || 0;
}
if (document.body) {
x2 = document.body.scrollLeft || 0;
y2 = document.body.scrollTop || 0;
}
x3 = window.scrollX || 0;
y3 = window.scrollY || 0;
var x = Math.max(x1, Math.max(x2, x3));
var y = Math.max(y1, Math.max(y2, y3));
window.scrollTo(Math.floor(x / 2), Math.floor(y / 2));
if (x > 0 || y > 0) {
window.setTimeout("backToTop()", 25);
}
}

function conf_topics(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='topics_manage.php';
		document.delform.submit();
	}
}

function conf_event(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='event_manage.php';
		document.delform.submit();
	}
}

function conf_file(id,nm,eid) {
	if (confirm("「 "+nm+" 」 の画像を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.id.value=eid;
		document.delform.action='edit_file.php';
		document.delform.submit();
	}
}
function conf_file2(id,nm,eid) {
	if (confirm("「 "+nm+" 」 を削除します。本当に宜しいですか？")) {
		document.delform2.target='_parent';
		document.delform2.delete_id.value=id;
		document.delform2.id.value=eid;
		document.delform2.action='edit_file.php';
		document.delform2.submit();
	}
}

function conf_entry(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='entry_manage.php';
		document.delform.submit();
	}
}

function conf_user(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='user_manage.php';
		document.delform.submit();
	}
}

function conf_pub(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='pub_manage.php';
		document.delform.submit();
	}
}

function conf_activity(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='activity_manage.php';
		document.delform.submit();
	}
}

function conf_kesseki(id,nm) {
	if (confirm("「 "+nm+" 」を削除します。本当に宜しいですか？")) {
		document.delform.target='_parent';
		document.delform.delete_id.value=id;
		document.delform.action='kesseki.php';
		document.delform.submit();
	}
}
