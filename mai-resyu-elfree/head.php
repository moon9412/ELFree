<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja" dir="ltr">
<head>
    <meta http-equiv="Content-Language" content="ja">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="index,follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <? /*title,meta切り替え---*/ $url = $_SERVER['SCRIPT_NAME']; ?>

    <? /*トップページ*/ if($url == "/"){ ?>
    <title>シニアエンジニア向け年齢不問案件の紹介はSEES</title>
    <meta name="description" content="SEESは年齢不問案件が中心の、ITエンジニア向け案件紹介サイトです。50代、60代の方も活躍しております。">
    <meta name="keywords" content="年齢不問,フリーランス,SOHO,エンジニア,案件,求人,仕事,システム,プログラム">

    <? /*SEESとは*/ }else if($url == "/about.html"){ ?>
    <title>ITプロジェクトと技術者を結びつけるSEESとは｜SEES</title>
    <meta name="description" content="SEESはIT業界に精通した専任スタッフが、あなたの詳細なスキルやご要望をお伺いし、ご要望に沿った案件をご提案するサービスです。">
    <meta name="keywords" content="IT,出張登録,エンジニア,求人,仕事,案件,システム,プログラム">

    <? /*就業までのフロー*/ }else if($url == "/flow.html"){ ?>
    <title>就業までのフロー｜SEES</title>
    <meta name="description" content="求人案件の検索から、就業までの流れをご説明。">
    <meta name="keywords" content="IT,出張登録,エンジニア,求人,仕事,案件,システム,プログラム">

    <? /*運営会社*/ }else if($url == "/company.html"){ ?>
    <title>運営会社｜SEES</title>
    <meta name="description" content="SEESを運営する株式会社Miraieについてのご紹介。">
    <meta name="keywords" content="IT,求人,検索,エンジニア">

    <? /*プライバシーポリシー*/ }else if($url == "/privacypolicy.html"){ ?>
    <title>プライバシーポリシー｜SEES</title>
    <meta name="description" content="SEESを運営する株式会社Miraieについてのご紹介。">
    <meta name="keywords" content="IT,求人,検索,エンジニア">

    <? /*紹介キャンペーン*/ }else if($url == "/campaign2016.html"){ ?>
    <title>SEESでは紹介キャンペーンを実施しています！｜SEES</title>
    <meta name="description" content="SEESを運営する株式会社Miraieについてのご紹介。">
    <meta name="keywords" content="IT,求人,検索,エンジニア">

    <? /*お問い合わせ*/ }else if($url == "/inquiry.html"){ ?>
    <title>お問い合わせ｜SEES</title>
    <meta name="description" content="SEESに対してのお問い合わせフォーム。">
    <meta name="keywords" content="IT,求人,検索,登録面談,エントリー">

    <? /*お問い合わせ確認*/ }else if($url == "/kakunin.html"){ ?>
    <title>お問い合わせ入力内容のご確認｜SEES</title>
    <meta name="description" content="SEESに対してのお問い合わせフォーム。">
    <meta name="keywords" content="IT,求人,検索,登録面談,エントリー">

    <? /*お問い合わせ完了*/ }else if($url == "/thanks.html"){ ?>
    <title>お問い合わせありがとうございました｜SEES</title>
    <meta name="description" content="SEESに対してのお問い合わせフォーム。">
    <meta name="keywords" content="IT,求人,検索,登録面談,エントリー">

    <? /*登録面談受付中*/ }else if($url == "/touroku.html"){ ?>
    <title>登録面談受付中｜SEES</title>
    <meta name="description" content="IT業界に精通した専任スタッフが、あなたの詳細なスキルやご要望をお伺いし、ご要望に沿った案件をご提案いたします。出張登録制度あり。">
    <meta name="keywords" content="IT,求人,検索,登録面談,出張登録">

    <? /*登録面談確認*/ }else if($url == "/touroku_kakunin.html"){ ?>
    <title>登録面談入力内容のご確認｜SEES</title>
    <meta name="description" content="IT業界に精通した専任スタッフが、あなたの詳細なスキルやご要望をお伺いし、ご要望に沿った案件をご提案いたします。出張登録制度あり。">
    <meta name="keywords" content="IT,求人,検索,登録面談,出張登録">

    <? /*登録面談完了*/ }else if($url == "/touroku_thanks.html"){ ?>
    <title>登録面談のお申し込みありがとうございました｜SEES</title>
    <meta name="description" content="IT業界に精通した専任スタッフが、あなたの詳細なスキルやご要望をお伺いし、ご要望に沿った案件をご提案いたします。出張登録制度あり。">
    <meta name="keywords" content="IT,求人,検索,登録面談,出張登録">

    <? /*スキルシート登録*/ }else if($url == "/skill.html"){ ?>
    <title>スキルシート登録｜SEES</title>
    <meta name="description" content="スキルシートをアップしていただくだけで、最適な案件をご紹介いたします。">
    <meta name="keywords" content="IT,求人,検索,登録面談,スキルシート">

    <? /*スキルシート登録確認*/ }else if($url == "/skill_kakunin.html"){ ?>
    <title>スキルシート入力内容のご確認｜SEES</title>
    <meta name="description" content="スキルシートをアップしていただくだけで、最適な案件をご紹介いたします。">
    <meta name="keywords" content="IT,求人,検索,登録面談,スキルシート">

    <? /*スキルシート登録完了*/ }else if($url == "/skill_thanks.html"){ ?>
    <title>スキルシートのご登録ありがとうございました｜SEES</title>
    <meta name="description" content="スキルシートをアップしていただくだけで、最適な案件をご紹介いたします。">
    <meta name="keywords" content="IT,求人,検索,登録面談,スキルシート">

    <? /*list 新着、検索*/ }else{ ?>
    <title>新着エンジニア向け求人案件｜SEES</title>
    <meta name="robots" content="noindex,nofollow">
    <meta name="description" content="シニアエンジニア向けの求人案件一覧。言語やスキル、勤務地から検索。">
    <meta name="keywords" content="IT,求人,検索,エンジニア">
    <? } /*---title,meta切り替え終了*/ ?>

    <!-- load JavaScript -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="resource/script/bootstrap.js"></script>
    <script type="text/javascript" src="resource/script/common.js"></script>
    <script type="text/javascript" src="//platform.twitter.com/widgets.js" defer="defer"></script>
    <script type="text/javascript" src="resource/script/jkl-calendar.js" charset="UTF-8"></script>
    <script type="text/javascript">
    var cal = new JKL.Calendar("calid","formid","colname");
    var cal2 = new JKL.Calendar("calid2","formid","colname2");
    </script>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="resource/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript">
    $(function() {
        // Twitter
        function date_format(date){
            function format(str){
                return ("00" + str).slice(-2);
            }
            var array = date.split(" ");
            date = new Date(array[1] + " " + array[2] + ", " + array[5] + " " + array[3]);
            date.setHours(date.getHours() + 9);
            return date.getFullYear()  + "/" +
                   format(date.getMonth() + 1) + "/" +
                   format(date.getDate())      + " " +
                   format(date.getHours())     + ":" +
                   format(date.getMinutes());
        }
        $.getJSON('http://twitter.com/statuses/user_timeline/miraie_sees.json?count=3&callback=?', function (json) {
            for(var i in json){
                var text = json[i].text;
                var created_at = json[i].created_at;
                var twdate = date_format(created_at);
                text = text.replace(/(https?:\/\/[-a-z0-9._~:\/?#@!$&amp;amp;amp;\'()*+,;=%]+)/ig,'<a href="$1" target="_blank">$1</a>').replace(/@+([_A-Za-z0-9-]+)/ig,'<a href="http://twitter.com/$1" target="_blank">@$1</a>').replace(/#+([_A-Za-z0-9-]+)/ig,'<a href="http://search.twitter.com/search?q=$1" target="_blank">#$1</a>');
                $("#twitterArea").append('<span class="fwb txt10">' + twdate + '</span>' + '<p class="">' + text + '</p>');
            }
        });

        });
    </script>
</head>