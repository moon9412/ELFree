/* Accordion */
$(function(){
    $('header nav li').hover(function(){
        $("ul.child:not(:animated)",this).slideDown();
    }, function(){
        $("ul.child",this).slideUp();
    });
});
$(function(){
    $('.navi-sp-list .down-sp').click(function() {
        $(this).next().slideToggle();
    });
    $('.search_down').click(function() {
        $(this).next().slideToggle();
    });
});

/* Clear Form */
$(function(){
	$(".clearForm").bind("click", function(){
	    $(this.form).find("textarea, :text, select").val("").end().find(":checked").prop("checked", false);
	});
});

$(function() {
  var pagetop = $('.backtotop');
  pagetop.click(function () {
    $('html, body').animate({ scrollTop: 0 }, 700);
        return false;
  });
});
