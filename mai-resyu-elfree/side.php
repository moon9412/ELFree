<div class="sidebar">
	<ul class="row">
		<li class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
			<a href="skill.html" class="skill_btn">
				スキルシートご登録
				<span>スキルシートのご登録後、<br>24時間以内にご返答します。</span></a>
		</li>
		<li class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
			<a href="touroku.html" class="touroku_btn">
				登録面談受付中
				<span>気になるプロジェクトを個別に<br>ご説明いたします。</span>
			</a>
		</li>
		<li class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
			<a href="campaign2016.html"><img src="resource/img_common/side4.png" alt="スキルシート登録"></a>
		</li>
		<li class="col-lg-12 col-md-12 col-sm-6 col-xs-12 twitter-box">
			<a class="twitter-timeline" href="https://twitter.com/miraie_sees" data-widget-id="294351229168009218">@miraie_sees からのツイート</a>
			<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</li>
		<li class="col-lg-12 col-md-12 col-sm-6 col-xs-12 miraie-logo">
			<a href="http://www.miraie-group.jp/" target="_blank"><img src="resource/img_common/side3.png" alt="Miraie"></a>
		</li>
	</ul>
</div>