	<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-37830642-1']);
	_gaq.push(['_trackPageview']);

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

	</script>

	<header>
			<div class="container head1">
				<div class="top-left">
					<ul>
						<li class="logo">
						<a href="./"><img src="resource/img_common/logo.png" alt="シニアエンジニア向けの求人案件検索サイト｜SEES"></a>
						</li>
						<li class="logo-txt">フリーランスの女性を応援する<br>求人サイトELFree(エルフリ）</li>
					</ul>
				</div>
				<div class="top-right">
					<div>
					<ul class="tel">
						<li class="tel-num">03-5774-6300</li>
						<li class="eigyou">営業時間 9:30－18:30（土日祝除く）</li>
						<li><a href="inquiry.html" class="inquiry">お問い合わせ</a></li>
					</ul>
					<nav class="hidden-sm hidden-xs">
						<ul>
							<li><a href="./">TOPページ</a></li>
							<li><a href="list.html">新着求人案件</a></li>
							<li><span class="down">求人情報をみる</span>
								<ul class="child">
									<li><a href="list.html?potision[]=2">プログラマ</a></li>
									<li><a href="list.html?potision[]=3">システムエンジニア</a></li>
									<li><a href="list.html?potision[]=4">アーキテクト・PM・PL</a></li>
									<li><a href="list.html?potision[]=8">フロントエンジニア</a></li>
									<li><a href="list.html?potision[]=5">デザイナー・ディレクター</a></li>
									<li><a href="list.html?potision[]=6">インフラエンジニア</a></li>
									<li><a href="list.html?potision[]=7">その他</a></li>
								</ul>
							</li>
							<li><a href="about.html">ELFreeとは</a></li>
							<li><a href="flow.html">就業までのフロー</a></li>
						</ul>
					</nav>
					<!--nav sp-->
					<nav class="collapse navi-sp-list" id="collapseNavi-sp">
						<ul>
							<li><a href="./">TOPページ</a></li>
							<li><a href="list.html">新着求人案件</a></li>
							<li><span class="down-sp">求人情報をみる</span>
								<ul class="child-sp">
									<li><a href="list.html?potision[]=2">プログラマ</a></li>
									<li><a href="list.html?potision[]=3">システムエンジニア</a></li>
									<li><a href="list.html?potision[]=4">アーキテクト・PM・PL</a></li>
									<li><a href="list.html?potision[]=8">フロントエンジニア</a></li>
									<li><a href="list.html?potision[]=5">デザイナー・ディレクター</a></li>
									<li><a href="list.html?potision[]=6">インフラエンジニア</a></li>
									<li><a href="list.html?potision[]=7">その他</a></li>
								</ul>
							</li>
							<li><a href="about.html">SEESとは</a></li>
							<li><a href="flow.html">就業までのフロー</a></li>
							<li><a href="company.html">運営会社</a></li>
							<li><a href="privacypolicy.html">プライバシーポリシー</a></li>
							<li class="inquiry-btn"><a href="inquiry.html">お問い合わせ</a></li>
							<li class="camp-btn"><a href="campaign2016.html">紹介キャンペーン</a></li>
							<li class="skill-btn"><a href="skill.html">スキルシート登録</a></li>
							<li class="regi-btn"><a href="touroku.html">登録面談受付中</a></li>
							<li class="close-btn" data-toggle="collapse" data-target="#collapseNavi-sp" aria-expanded="true" aria-controls="collapseNavi-sp"><span>×閉じる</span></li>
						</ul>
					</nav>
				</div>
			</div>
			</div>
	</header>
