<footer>
    <div class="container">
        <div class="backtotop">
            <a href="">Page Top</a>
        </div>
    </div>
    <div class="footer-top">
        <div class="container">
            <ul>
                <li class="logo">
                    <a href="./"><img src="resource/img_common/0120logo.png" alt="シニアエンジニア向けの求人案件検索サイト｜SEES"></a>
                </li>
                <li class="logo-txt">
                    <p>40歳以上のシニアエンジニアのための求人サイト＜シーズ＞</p>
                    <p>年齢不問の案件が多数！シニアエンジニア向けの求人案件検索サイト、随時更新中！</p>
                </li>
                <li class="tel">
                    <p class="tel-num">03-5774-6300</p>
                    <p class="eigyou">営業時間 9:30-18:00(土日祝除く)</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer-middle">
        <div class="container">
            <ul>
                <li class="footer-address">
                    <address>
                        <dl>
                            <dt>株式会社Miraie</dt>
                            <dd>〒150－0002</dd>
                            <dd>東京都渋谷区渋谷1－12－2 クロスオフィス渋谷6F</dd>
                            <dd>TEL 03-5774-6300　FAX 03-5774-6301</dd>
                        </dl>
                    </address>
                </li>
                <li class="footer-nav">
                    <dl>
                        <dt>Service</dt>
                        <dd>
                            <ul>
                                <li><a href="./">TOPページ</a></li>
                                <li><a href="list.html">新着求人案件</a></li>
                                <li><a href="about.html">SEESとは</a></li>
                                <li><a href="flow.html">就業までのフロー</a></li>
                                <li><a href="company.html">運営会社</a></li>
                                <li><a href="privacypolicy.html">プライバシーポリシー</a></li>
                                <li><a href="inquiry.html">お問い合わせ</a></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p>Copyright(C) Miraie Co., Ltd. All Rights Reserved.</p>
        </div>
    </div>
</footer>