<?
// ------------------------------------------
// データベース接続
//------------------------------------------
$hostname = "mysql430.db.sakura.ne.jp";
$username = "miraiekouryu";
$password = "pm6wzrpzga";
$dbname = "miraiekouryu_recruit";

$db = null;

header('Content-type: text/html; charset=utf-8');

try {
	$db = new PDO ( "mysql:host=$hostname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'") );
} catch ( PDOException $e ) {
	echo "データベースに接続できません。";
	exit ();
}

if (! $db->query ( "use " . $dbname )) {
	echo "データベースが存在しません。";
	exit ();
}



// ------------------------------------------
//日付設定
//------------------------------------------
$today=mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
$newday=mktime(date("H"),date("i"),date("s"),date("m"),date("d")-7,date("Y"));

//------------------------------------------
//各種読み込み
//------------------------------------------

$seid="kyujin";
$sepw="pass";
$send="itc-sales@miraie-group.jp";

//公開フラグ
$flag = "公開";

//ページ数
$pagekensu=10;

//カンマ表示
if (!function_exists(num_comma)){
  function num_comma($num, $keta = 2){
    $num1 = floor($num);
    $s = number_format($num1);
    If ($keta>0)  $s .= substr(strstr($num, "."),0, $keta+1);
    return $s;
  }
}

function getRandomString($nLengthRequired = 8){
    $sCharList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    mt_srand();
    $sRes = "";
    for($i = 0; $i < $nLengthRequired; $i++)
        $sRes .= $sCharList[mt_rand(0, strlen($sCharList) - 1)];
    return $sRes;
}

// magic_quotes_gpc = On の場合の対策
if (get_magic_quotes_gpc()) {
  function strip_magic_quotes_slashes($arr)
  {
    return is_array($arr) ?
      array_map('strip_magic_quotes_slashes', $arr) :
      stripslashes($arr);
  }

  $_GET     = strip_magic_quotes_slashes($_GET);
  $_POST    = strip_magic_quotes_slashes($_POST);
  $_REQUEST = strip_magic_quotes_slashes($_REQUEST);
  $_COOKIE  = strip_magic_quotes_slashes($_COOKIE);
}

//FILESの内容取得
function Files($Prm1,$Prm2) {
return $_FILES[$Prm1][$Prm2];
}

//ディレクトリ内　ファイル削除
function unlinkRecursive($dir, $deleteRootToo)
{
    if(!$dh = @opendir($dir))
    {
        return;
    }
    while (false !== ($obj = readdir($dh)))
    {
        if($obj == '.' || $obj == '..')
        {
            continue;
         }  

        if (!@unlink($dir . '/' . $obj))
        {
           unlinkRecursive($dir.'/'.$obj, true);
       }
   }

   closedir($dh);
  
   if ($deleteRootToo)
   {
       @rmdir($dir);
   }
  
   return;
}

//コードページ
$DbCode="UTF-8"; //データベース登録時の変換コード
$PgCode="UTF-8";  //プログラム上で文字を判定する際の変換コード(Site用)
 
function Head($Prm,$Len) {
//グローバル変数の定義
global $PgCode;
global $DbCode;

$aryLine=explode("\n",mb_convert_encoding($Prm,$PgCode,$DbCode));

$Tmp=$aryLine[0];
if(substr($Tmp,strlen($Tmp)-strlen("\n"),strlen("\n"))=="\n") {
$Tmp=substr($Tmp,0,strlen($Tmp)-strlen("\n")); 
}
if(mb_strlen($Tmp,$PgCode)>($Len-1)) {
$Tmp=mb_substr($Tmp,0,($Len-1),$PgCode)."…";
}
return $Tmp;
}


?>